import os
import morphonet

"""
Through the morphonet API, load 3d labeled images and export the surface mesh.

Useful links:
The morphonet api git:  https://gitlab.inria.fr/MorphoNet/morphonet_api/-/blob/main/morphonet/tools.py?ref_type=heads
Morphonet installation: https://pypi.org/project/morphonet/
"""


def main():
    astec_samp = "Astec-Pm8"

    mesh_export_path = f"{astec_samp}/SURFACE_OBJ"
    create_directory(mesh_export_path)

    begin = 1
    end = 1

    for t in range(begin, end + 1):
        segmented_file = f"{astec_samp}/POST/POST_RELEASE/{astec_samp}_intrareg_post_t{t:03d}.nii.gz"        

        mesh_obj_str = generate_surface_meshes(segmented_file)

        exported_mesh_name = f"{mesh_export_path}/{astec_samp}_intrareg_post_t{t:03d}"
        save_data(mesh_obj_str, exported_mesh_name, "obj")


def save_data(data, file_name, extension):
    """
    Save data with target extension.

    Parameters
    ----------
    data : any
        Data that will be saved.
    file_name : str
        Name used for the saved data.
    extension : str
        Extension added at the end of file name.
    """
    with open(f"{file_name}.{extension}", "w") as f:
        f.write(data)


def create_directory(directory):
    """
    Create a directory if it does not exist

    Parameters
    ----------
    directory : str
        The directory that will be created
    """
    if not os.path.exists(directory):
        os.makedirs(directory)


def generate_surface_meshes(labeled_image_path):
    """
    Load labelled image and export the surface mesh.

    Parameters
    ----------
    labeled_image_path : str
        Path to the labelled image.

    Returns
    -------
    str
        Single string with the mesh information in OBJ format
    """
    segmented_img = morphonet.tools.imread(labeled_image_path)
    segmented_img[segmented_img != 1] = 2

    obj = morphonet.tools.convert_to_OBJ(
        segmented_img, factor=4, background=1
    )  # Convert 3D Images in Meshes

    return obj


if __name__ == "__main__":
    main()
